# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# docker settings
export DOCKER_BUILDKIT=0

# Set local
export LC_ALL=en_US.UTF-8

# If you come from bash you might have to change your $PATH.
export GOPATH=$HOME/go
export GOROOT=/usr/local/opt/go/libexec
export PATH=$HOME/bin:${GOPATH//://bin:}/bin:/usr/local/bin:/usr/local/sbin:$PATH
export PATH="$HOME/.embulk/bin:$PATH"
export PATH="/usr/local/opt/openjdk/bin:$PATH"
export PATH="~/go/bin:$PATH"
export PATH="/Users/sascha/Library/Python/3.7/bin:${PATH}"
export PATH="/usr/local/opt/gnu-getopt/bin:$PATH"

# Source sensitive information
[[ -f ~/.zshrc_sensitive ]] && . ~/.zshrc_sensitive

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="robbyrussell"
# ZSH_THEME="agnoster"
ZSH_THEME=powerlevel10k/powerlevel10k

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

export HISTFILESIZE=1000000000
export HISTSIZE=1000000000

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  aws
  brew
  dotenv
  git
#  vagrant
  golang
  terraform
  kubectl
  helm
  docker
#  docker-compose
  magic-enter
  colorize
  aws-vault
  gpg-agent
#  ssh-agent
  pyenv
)

# ssh-agent plugin setting
#zstyle :omz:plugins:ssh-agent agent-forwarding on
#zstyle :omz:plugins:ssh-agent identities sascha_privat id_rsa

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias guid="pwsh -c '[System.Guid]::NewGuid()'"
#alias cat="bat -p"
alias config='/usr/local/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias kx=kubectx
alias kns=kubens
alias kc=kubecm
alias kgj="kubectl get jobs"
alias kdj="kubectl describe jobs"
alias kdelj="kubectl delete jobs"
alias kgcj="kubectl get cj"
alias kdcj="kubectl describe cj"
alias kdelcj="kubectl delete cj"
alias kgpd="kubectl get pod -o=\"custom-columns=Name:.metadata.name,Image:.spec.containers[*].image\""
alias kgperr="kubectl get pods -A | grep -vE '(Completed|Running)'"
alias kdelperr="kubectl get pods -A | awk '!/Running|Completed|STATUS/{ system(\"kubectl delete pod \" \$2 \" -n \" \$1) }'"
alias kgn="kubectl get nodes"
alias kgpcu="kubectl get pod -o=\"custom-columns=Name:.metadata.name,PodIP:status.podIP,PodPort:.spec.containers[*].ports[*].containerPort,Node:.spec.nodeName\""
alias krbash="kubectl run --generator=run-pod/v1  -ti --rm --image ubuntu -- debug-bash-sascha"
alias tg="terragrunt"
alias tgenv="terraenv terragrunt"
alias tf="terraform"
alias tfenv="terraenv terraform"
alias dkc="docker kill \$(docker ps |/usr/local/bin/awk 'NR>1 { print \$1 }')"
alias ddc="docker rm -f \$(docker ps -a|/usr/local/bin/awk 'NR>1 { print \$1 }')"
alias ddi="docker image rm -f \$(docker images -a|/usr/local/bin/awk 'NR>1 {print \$3}')"
alias ddv="docker volume rm \$(docker volume ls|/usr/local/bin/awk 'NR>1 {print \$2}')"
alias ubuntu="docker run -v \$(pwd)/:/local_drive -e AWS_REGION=\${AWS_REGION} -e AWS_ACCESS_KEY_ID=\${AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=\${AWS_SECRET_ACCESS_KEY} -e AWS_SESSION_TOKEN=\${AWS_SESSION_TOKEN} -e AWS_SESSION_EXPIRATION=\${AWS_SESSION_EXPIRATION} -ti ubuntu"
alias flushdns="sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder"
alias ff="find ./ -type f -name $1"
alias fd="find ./ -type d -name $1"
alias ecr-login-prod="aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 211841842200.dkr.ecr.eu-central-1.amazonaws.com"
alias ecr-login-test="aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 544416603772.dkr.ecr.eu-central-1.amazonaws.com"
alias deploy-demo="ansible-playbook --vault-password-file ~/.vault_password create-demo-system.yml"

# Set default user for oh-my-zsh
DEFAULT_USER="sascha"

export PYENV_VIRTUALENV_DISABLE_PROMPT=1
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Enable ssh-agent and add ssh keys
# eval `ssh-agent`
#for key in $( file ~/.ssh/*|grep "private key"|cut -d":" -f1 ); do
#    ssh-add $key;
#done

source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# aws-vault settings
AWS_ASSUME_ROLE_TTL=1h

# aws-vault functions
ax() {
  local AWS_VAULT_PROFILE=$1
  [[ -z "$AWS_VAULT_PROFILE" ]] && { AWS_VAULT_PROFILE=$(aws-vault list --profiles | fzf) }

  local AWS_VAULT_PARAMS
  [[ ${#@} > 1 ]] && { shift; AWS_VAULT_PARAMS=$@ }

  aws-vault exec $AWS_VAULT_PROFILE -- $AWS_VAULT_PARAMS
}

al() {
  local AWS_VAULT_PROFILE=$1
  [[ -z "$AWS_VAULT_PROFILE" ]] && { AWS_VAULT_PROFILE=$(aws-vault list --profiles | fzf) }

  aws-vault login $AWS_VAULT_PROFILE
}

# kubectl functions
kgpip() {
  NAMESPACE=$1
  POD=$2

  if [[ -z "$NAMESPACE" ]]; then
    echo "Provide namespace as first argument"
    return
  fi

  if [[ -z "$POD" ]]; then
    echo "Provide pod name as second argument"
    return
  fi

  kgp -n $NAMESPACE $POD -o json | jq '. | {Pod: .metadata.name, Namespace: .metadata.namespace, PodIP: .status.podIP, HostIP: .status.hostIP }'
}

kgpallips() {
  for NAMESPACE POD READY STATUS RESTARTS AGE in $( kc get pods -A |grep 'Running' ); do
    kgp -n $NAMESPACE $POD -o json | jq '. | { Pod: .metadata.name, Namespace: .metadata.namespace, PodIP: .status.podIP, HostIP: .status.hostIP }'
  done
}

# AWS
export AWS_REGION=eu-central-1

aws-login(){
    PROFILE=$( aws configure list-profiles | fzf )
    REGION='eu-central-1'

    export AWS_REGION=$REGION
    export AWS_PROFILE=$PROFILE
    aws sso login --profile $PROFILE
}

aws-reset-env() {
    unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN
    aws sso logout
}

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Pyenv Initialization
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# Jenv Initialization
  export PATH="$HOME/.jenv/bin:$PATH"
  eval "$(jenv init -)"

